<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
<style>
h1 {
	text-align: center;
	font-family:'SimSun',monospace;
}
.errorMessages{
color: #FF0000;
}
body {
	background: #CCCCCC;
}

ul li {
	list-style: none;
}

ul {
	width: 230px;
	margin: 0 auto;
}

label {
	margin-right: 10px;
	width: 130px;
	float: left;
}
h5{
text-align:right;
margin-right:50px;
}
p{
position: absolute;
	top: 0%;
	right: 10%;
}
</style>
</head>
<body>
	<h1>ユーザー登録</h1>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><nobr><c:out value="${message}" /></nobr>
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<ul>
				<li><label for="name">名前</label><br /> <input name="name"
					id="name"  /></li>
				<li><label for="login_id">ID</label><br /> <input name="login_id"
					id="login_id"  /></li>
					<li><label for="branch_id" >支店名</label><br />
					<select size=1 style="width:175px;" name="branch_id" >
				<c:forEach var="branch" items="${branch}">
					<option value="<c:out value="${branch.key}" />"
					<c:if test="${editUser.branchId == branch.key}">selected</c:if>>
					<c:out value="${branch.value}" />
					</option>
				</c:forEach>
				</select></li>
				<li><label for="department_id" >部署・役職</label><br />
				<select size=1 style="width:175px;" name="department_id" >
				<c:forEach items="${department}" var="department">
					<option value="<c:out value="${department.key}" />" <c:if test="${editUser.departmentId == department.key}">selected</c:if>><c:out value="${department.value}" /></option>
				</c:forEach>
				</select>
				<li><label for="password">pass</label> <br /> <input
					name="password" type="password" id="password"  /></li>
					<li><label for="password_confirm">pass（確認用）</label> <br /> <input
					name="password_confirm" type="password" id="password_confirm"  /></li>
				<ul>
				<li><input type="submit" value="登録" /> <br /></li>
				<p><a href="userManagement">戻る</a></p>
				</ul>
				</ul>
		</form>
		<h5><div class="copyright">Copyright(c)Eguchi_Naok</div></h5>
	</div>
</body>
</html>