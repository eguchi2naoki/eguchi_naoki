<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー情報編集</title>
<style>
body {
	background: #CCCCCC;
}

.errorMessages {
	color: #FF0000;
}

label {
	margin-right: 30px;
	width: 100px;
	float: left;
}

h1 {
	text-align: center;
	font-family: 'SimSun', monospace;
}

p {
	position: absolute;
	top: 0%;
	right: 10%;
}

h5 {
	text-align: right;
	margin-right: 50px
}

ul {
	width: 500px;
	margin: 0 auto;
}

ul li {
	list-style: none;
}
</style>
</head>
<body>
	<h1>ユーザー情報編集</h1>
	<div class="container">
		<div class="row">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul class="error">
						<c:forEach items="${errorMessages}" var="message">
							<li class="validate"><c:out value="${message}" /></li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<p>
				<a href="userManagement">戻る</a>
			</p>
			<form action="userediting" method="post">
				<ul>
					<li><label for="login_id">ログインID</label> <input id="login_id"
						name="login_id" value="${editUser.loginId}" /></li>

					<li><label for="name">名前</label> <input id="name" name="name"
						value="${editUser.name}" /></li>

					<li><label for="branch_id">支店名</label> <select size=1
						style="width: 175px;" name="branch_id">
							<c:forEach var="branch" items="${branch}">
								<option value="${branch.key}"
									<c:if test="${editUser.branchId == branch.key}">selected</c:if>>
									${branch.value}</option>
							</c:forEach>
					</select></li>

					<li><label for="department_id">部署・役職</label> <select size=1
						style="width: 175px;" name="department_id">
							<c:forEach var="department" items="${department}">
								<option value="${department.key}"
									<c:if test="${editUser.departmentId == department.key}">selected</c:if>>${department.value}</option>
							</c:forEach>
					</select></li>

					<li><label for="password">パスワード</label> <input id="password"
						name="password" type="password" /></li>

					<li><label for="password_confirm"><nobr>パスワード(確認)</nobr></label>
						<input id="password_confirm" name="password_confirm"
						type="password" /></li>

					<input type="hidden" name="id" value="${userId}" />
					<li><input type="submit" value="登録" /></li>
				</ul>
			</form>
		</div>
	</div>
	<h5>
		<div class="copylight">Copyright(c)Eguchi_Naoki</div>
	</h5>
</body>
</html>