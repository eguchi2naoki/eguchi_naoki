<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板</title>
<style>
body {
	background: #CCCCCC;
}

.back {
	background: #EEEEEE
}

.errorMessages {
	color: #FF0000;
}

h1 {
	text-align: center;
	font-family: 'SimSun', monospace;
}

h2 {
	text-align: center;
	font-family: 'SimSun', monospace;
}

.search {
	text-align: center;
	background: #EEEEEE;
	font-weight: 700;
	padding: 20px;
}

h3 {
	text-align: center;
	padding: 20pt
}

li {
	text-align: center;
	list-style: none;
	margin-right: 20pt
}

.menu {
	position: absolute;
	top: 0%;
	right: 10%;
}

h5 {
	text-align: right;
	margin-right: 50px
}

span {
	padding-right: 5px
}
</style>
</head>
<body>

	<h1>業務連絡掲示板</h1>

	<ul>
		<c:if test="${ empty loginUser }">
			<li><input type="button" onclick="location.href='login'"
				value="ログイン"></li>
		</c:if>
	</ul>


	<c:if test="${ not empty loginUser }">
		<h2>
			ようこそ
			<c:out value="${loginUser.name}" />
			様
		</h2>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul class="error">
					<c:forEach items="${errorMessages}" var="message">
						<li class="validate"><c:out value="${message}" /></li>
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>


		<div class="search">
			投稿絞り込み<br />
			<form action="./" method="post" role="search" id="search">
				カテゴリー<input type="text" name="category" value="${editCategory}">
				開始日<input type="date" name="from" value="${editFrom}">～ 終了日<input
					type="date" name="to" value="${editTo}">
				<button type="submit">検索</button>
			</form>
		</div>

		<div class="menu">
			<span><a href="newContribution">新規投稿</a></span>
			<span><a href="userManagement">ユーザー管理</a></span>
			<span><a href="./">戻る</a></span>
			<span><a href="logout">ログアウト</a></span>
		</div>


		<div class="back">
			<c:forEach items="${contribution}" var="contribution">
				<div class="contribution">
					<form action="deletePost" method="post">
						<div class="name">
							投稿者名：
							<c:out value="${contribution.name}" />
						</div>
						<div class="title">
							件名：
							<c:out value="${contribution.title}" />
						</div>
						<div class="category">
							カテゴリー:
							<c:out value="${contribution.category}" />
						</div>
						<div class="text">
							本文：
							<c:out value="${contribution.text}" />
						</div>
						<div class="date">
							投稿日時：
							<fmt:formatDate value="${contribution.created_date}"
								pattern="yyyy/MM/dd HH:mm:ss" />
							<c:if test="${contribution.userId == loginUser.id }">
								<input type="submit" value="削除"
									onclick="return confirm('削除してもよろしいですか？');" />
							</c:if>
						</div>
						<input name="id" type="hidden"
							value="<c:out value="${contribution.id}" />" />
					</form>
				</div>

				<form action="comment" method="post">
					<input name="contributionId" type="hidden"
						value="${contribution.id}">
					<textarea name="text" cols="50" rows="2" class="tweet-box"></textarea>
					<input type="submit" value="コメント">
				</form>

				<c:forEach items="${comment}" var="comment">
					<div class="comment">
						<form action="deleteComment" method="post">
							<c:if test="${contribution.id == comment.contributionId}">
								<div class="name">
									<c:out value="${comment.name}" />
								</div>
								<div class="text">
									<c:out value="${comment.text}" />
								</div>
								<div class="date">
									<fmt:formatDate value="${comment.created_date}"
										pattern="yyyy/MM/dd HH:mm:ss" />
									<c:if test="${comment.userId == loginUser.id }">
										<input type="submit" value="削除"
											onclick="return confirm('削除してもよろしいですか？');" />
									</c:if>
								</div>
								<input name="id" type="hidden"
									value="<c:out value="${comment.id}" />" />
							</c:if>
						</form>
					</div>
				</c:forEach>
				<hr>
			</c:forEach>
		</div>
	</c:if>

	<h5>
		<div class="copylight">Copyright(c)Eguchi_Naoki</div>
	</h5>

</body>
</html>