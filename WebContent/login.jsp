<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン</title>
<style>
body {
	background: #CCCCCC;
}

h1 {
	text-align: center;
	margin-left: 40px;
	font-family:'SimSun',monospace;
}
.errorMessages{
color: #FF0000;
}

ul li {
	list-style: none;
	text-align: center;
}

h5 {
	text-align: right;
	margin-right: 50px;
}

p {
	position: absolute;
	top: 0%;
	right: 10%;
}
</style>
</head>
<body>
	<h1>ログイン</h1>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="login" method="post">
			<br />
			<ul>
				<li><label for="login_id">ログインID</label><br /> <input
					name="login_id" id="login_id" /></li>

				<li><label for="password">パスワード</label><br /> <input
					name="password" type="password" id="password" /></li>

				<li><input type="submit" value="ログイン" /></li>
				<p>
					<a href="./">戻る</a>
				</p>
			</ul>
		</form>
		<h5>
			<div class="copyright">Copyright(c)Eguchi_Naoki</div>
		</h5>
	</div>
</body>
</html>