<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
<style>
body {
	background: #CCCCCC;
}

h1 {
	text-align: center;
	font-family:'SimSun',monospace;
}
.errorMessages{
color: #FF0000;
}
ul li {
	list-style: none;
}
textarea {
	resize: none;
}

h5 {
	text-align: right;
	margin-right: 50px;
}
p {
	position: absolute;
	top: 0%;
	right: 10%;
}
</style>
</head>
<body>
	<h1>新規投稿</h1>
<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
	<div class="errorMessages">
		<ul class="error">
			<c:forEach items="${errorMessages}" var="message">
				<li class="validate"> <c:out value="${message}" /></li>
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>

		<form action="newContribution" method="post">
		<label for="title">件名</label><br /> <input name="title"  id="title" /><br />
		 <label for="category">カテゴリ</label><br /> <input name="category"  id="category" /><br />
		<label for="text">本文</label><br /> <textarea name="text" cols="100" rows="10" class="tweet-box"></textarea>
		<br /> <label>投稿者：</label>${loginUser.name} 様<br />
		 <input type="submit" value="投稿">
			<p> <a href="./">戻る</a></p>
	</form>
	<h5>
		<div class="copyright">Copyright(c)Eguchi_Naoki</div>
	</h5>
	</div>
</body>
</html>