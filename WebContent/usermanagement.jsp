<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
<style>
body {
	background: #CCCCCC;
}

h1 {
	text-align: center;
	font-family: 'SimSun', monospace;
}

table {
	background-color: #EEEEEE;
}

h5 {
	text-align: right;
	margin-right: 50px;
}

p {
	position: absolute;
	top: 0%;
	right: 10%;
}
</style>
</head>
<body>
	<h1>ユーザー一覧</h1>
	<table border="1" align="center">
		<thead>
			<tr>
				<th>名前</th>
				<th>支店</th>
				<th>部署</th>
				<th>停止</th>
				<th>編集</th>
				<th>削除</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="user" items="${userList}">
				<tr>
					<td><c:out value="${user.name}" /></td>
					<td><c:out value="${user.branchName}" /></td>
					<td><c:out value="${user.departmentName}" /></td>
					<td><c:choose>
							<c:when test="${!user.isStopped()}">
								<c:set var="value" value="停止" />
							</c:when>
							<c:when test="${user.isStopped()}">
								<c:set var="value" value="復活" />
							</c:when>
						</c:choose>
						<form action="status" method="post">
							<input type="submit" value="<c:out value="${value}" />"
								onclick="return confirm('<c:out value="${value}" />してもよろしいですか？');" />
							<input name="id" type="hidden" value="${user.id}" /> <input
								name="is_stopped" type="hidden" value="${user.isStopped()}" />
						</form></td>
					<td>
						<form action="userediting" method="get">
							<input name="id" type="hidden" value="${user.id}" /> <input
								type="submit" value="編集" />
						</form>
					</td>
					<td>
						<form action="deleteUser" method="post">
							<input type="submit" value="削除"
								onclick="return confirm('削除してもよろしいですか？');" /> <input name="id"
								type="hidden" value="${user.id}" />
						</form>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<p>
		<a href="signup">ユーザー登録</a> <a href="./">戻る</a>
	</p>
	<h5>
		<div class="copyright">Copyright(c)Eguchi_Naoki</div>
	</h5>
</body>
</html>