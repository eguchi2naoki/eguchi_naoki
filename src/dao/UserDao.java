package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.ComplementUser;
import beans.User;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users( ");
			sql.append("login_id");
			sql.append(",branch_id");
			sql.append(",department_id");
			sql.append(", is_stopped");
			sql.append(",name");
			sql.append(",password");
			sql.append(") VALUES (");
			sql.append("?"); //login_id
			sql.append(", ?");//branch_id
			sql.append(", ?");//department_id
			sql.append(", ?"); // is_stopped
			sql.append(", ?"); //name
			sql.append(", ?"); //password
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1,  user.getLoginId());
			ps.setInt(2,  user.getBranchId());
			ps.setInt(3,  user.getDepartmentId());
			ps.setBoolean(4, user.isStopped());
			ps.setString(5, user.getName());
			ps.setString(6, user.getPassword());
			ps.executeUpdate();
		}catch (SQLException e) {
			throw new  SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String login_Id,
			String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_Id = ?  AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, login_Id);
			ps.setString(2, password);


			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getStopUser(Connection connection,String is_stopped) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE  is_stopped = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1,is_stopped);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<User> getAllUser(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty()) {
				return null;
			} else {
				return userList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<ComplementUser> getAllComplementUser(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "";
			sql += "SELECT";
			sql += "   users.*";
			sql += " , branch.name AS branch_name";
			sql += " , department.name AS department_name";
			sql += " FROM";
			sql += "   users";
			sql += " LEFT JOIN";
			sql += "   branch";
			sql += " ON";
			sql += "   users.branch_id = branch.id";
			sql += " LEFT JOIN";
			sql += "   department";
			sql += " ON";
			sql += "   users.department_id = department.id";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<ComplementUser> userList = toComplementUserList(rs);
			if (userList.isEmpty()) {
				return null;
			} else {
				return userList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_Id");
				int branch_id = rs.getInt("branch_id");
				int department_id  = rs.getInt("department_id");
				String name = rs.getString("name");
				String password = rs.getString("password");
				Boolean is_stopped=rs.getBoolean("is_stopped");

				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setBranchId(branch_id);
				user.setDepartmentId(department_id);
				user.setName(name);
				user.setPassword(password);
				user.setStopped(is_stopped);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	private List<ComplementUser> toComplementUserList(ResultSet rs) throws SQLException {
		List<ComplementUser> ret = new ArrayList<ComplementUser>();
		try {
			while (rs.next()) {
				ComplementUser user = new ComplementUser();
				user.setId(rs.getInt("id"));
				user.setLoginId(rs.getString("login_id"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranchId(rs.getInt("branch_id"));
				user.setStopped(rs.getBoolean("is_stopped"));
				user.setBranchName(rs.getString("branch_name"));
				user.setDepartmentId(rs.getInt("department_id"));
				user.setDepartmentName(rs.getString("department_name"));
				user.setName(rs.getString("name"));
				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void update(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			int idIndex = 5;
			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", department_id = ?");

			if (!user.getPassword().equals("")) {
				sql.append(", password = ?");
				idIndex = 6;
			}

			sql.append(" where id = ?");
			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranchId());
			ps.setInt(4, user.getDepartmentId());
			if (!user.getPassword().equals("")) {
				ps.setString(5, user.getPassword());
			}
			ps.setInt(idIndex, user.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, int id) {
		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM users WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void setStatus(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			String sql = "UPDATE users SET is_stopped = ? WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setBoolean(1, !user.isStopped());
			ps.setInt(2, user.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<Integer> getUserIdList(Connection connection, int branchId) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT id FROM users WHERE branch_id = ? AND department_id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, branchId);
			ps.setInt(2, 4);
			ResultSet rs = ps.executeQuery();
			List<Integer> userIdList = toUserIdList(rs);
			return userIdList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Integer> toUserIdList(ResultSet rs) throws SQLException {
		List<Integer> ret = new ArrayList<Integer>();
		try {
			while (rs.next()) {
				ret.add(rs.getInt("id"));
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public boolean otherGeneralExists(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT COUNT(*) AS count FROM users WHERE department_id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, 1);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt("count") <= 1) return false;
			}
			return true;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public boolean duplicateCheck(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT id FROM users WHERE login_id = ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1, user.getLoginId());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return false;
			}
			return true;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
