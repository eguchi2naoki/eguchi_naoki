package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comment.id as id,");
            sql.append("comment.text as text, ");
            sql.append("comment.user_id as user_id, ");
            sql.append("comment.contribution_id as contribution_id, ");
            sql.append("users.name as name, ");
            sql.append("comment.created_date as created_date ");
            sql.append("FROM comment ");
            sql.append("INNER JOIN users ");
            sql.append("ON comment.user_id = users.id ");
            sql.append("INNER JOIN contribution ");
            sql.append("ON comment.contribution_id = contribution.id ");
            sql.append("ORDER BY created_date ASC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
               String name = rs.getString("name");
               int id = rs.getInt("id");
               int userId = rs.getInt("user_id");
               int contributionId = rs.getInt("contribution_id");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserComment comment = new UserComment();
                comment.setName(name);
                comment.setId(id);
                comment.setUserId(userId);
                comment.setContributionId(contributionId);
                comment.setText(text);
                comment.setCreated_date(createdDate);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
