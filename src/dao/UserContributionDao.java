package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserContribution;
import exception.SQLRuntimeException;

public class UserContributionDao {

    public List<UserContribution> getUserContribution(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("contribution.id as id,");
            sql.append("contribution.text as text, ");
            sql.append("contribution.title as title, ");
            sql.append("contribution.category as category, ");
            sql.append("contribution.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("contribution.created_date as created_date ");
            sql.append("FROM contribution ");
            sql.append("INNER JOIN users ");
            sql.append("ON contribution.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserContribution> ret = toUserContributionList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserContribution> toUserContributionList(ResultSet rs)
            throws SQLException {

        List<UserContribution> ret = new ArrayList<UserContribution>();
        try {
            while (rs.next()) {
               String name = rs.getString("name");
               int id = rs.getInt("id");
               int userId = rs.getInt("user_id");
               String title = rs.getString("title");
               String category = rs.getString("category");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserContribution contribution = new UserContribution();
                contribution.setName(name);
                contribution.setId(id);
                contribution.setUserId(userId);
                contribution.setTitle(title);
                contribution.setCategory(category);
                contribution.setText(text);
                contribution.setCreated_date(createdDate);

                ret.add(contribution);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
