package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Contribution;
import beans.Search;
import beans.UserContribution;
import exception.SQLRuntimeException;

public class ContributionDao {

    public void insert(Connection connection, Contribution contribution) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO contribution ( ");
            sql.append("user_id");
            sql.append(",title");
            sql.append(",category");
            sql.append(", text");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?");//title
            sql.append(", ?");//category
            sql.append(", ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, contribution.getUserId());
            ps.setString(2, contribution.getTitle());
            ps.setString(3, contribution.getCategory());
            ps.setString(4, contribution.getText());


            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public List<Contribution> findAll(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "select * from contribution";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Contribution> contributionList = toContributionList(rs);
			return contributionList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

    public List<UserContribution> userContributionAll(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "";
			sql += "SELECT";
			sql += "  contribution.*";
			sql += ", users.name";
			sql += " FROM";
			sql += "  contribution";
			sql += " LEFT JOIN";
			sql += "  users";
			sql += " ON contribution.user_id = users.id";
			sql += " ORDER BY contribution.created_date DESC";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<UserContribution> usercontributionList = toUserContributionList(rs);
			return usercontributionList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

    private List<Contribution> toContributionList(ResultSet rs) throws SQLException {
		List<Contribution> ret = new ArrayList<Contribution>();
		try {
			while (rs.next()) {
				Contribution contribution = new Contribution();
				contribution.setId(rs.getInt("id"));
				contribution.setUserId(rs.getInt("user_id"));
				contribution.setTitle(rs.getString("subject"));
				contribution.setCategory(rs.getString("category"));
				contribution.setText(rs.getString("text"));
				contribution.setCreatedDate(rs.getDate("created_date"));
				contribution.setUpdatedDate(rs.getDate("updated_date"));
				ret.add(contribution);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

    private List<UserContribution> toUserContributionList(ResultSet rs) throws SQLException {
		List<UserContribution> ret = new ArrayList<UserContribution>();
		try {
			while (rs.next()) {
				UserContribution userContribution = new UserContribution();
				userContribution.setId(rs.getInt("id"));
				userContribution.setUserId(rs.getInt("user_id"));
				userContribution.setTitle(rs.getString("title"));
				userContribution.setText(rs.getString("text"));
				userContribution.setCategory(rs.getString("category"));
				userContribution.setName(rs.getString("name"));
				userContribution.setCreated_date(rs.getTimestamp("created_date"));
				ret.add(userContribution);
			}
			return ret;
		} finally {
			close(rs);
		}
		}

    public List<UserContribution> search(Connection connection, Search search) {
		PreparedStatement ps = null;
		try {
			String sql = "";
			sql += "SELECT";
			sql += "  contribution.*";
			sql += " , users.name";
			sql += " FROM";
			sql += "  contribution";
			sql += " INNER JOIN";
			sql += "  users";
			sql += " ON";
			sql += "  contribution.user_id = users.id";
			sql += " WHERE";
			sql += " contribution.id IN";
			sql += " (";
			sql += "	SELECT";
			sql += "	  id";
			sql += "	FROM";
			sql += "	  contribution";
			sql += "	WHERE";
			sql += "	  created_date BETWEEN";

			int count = 1;
			boolean fromFlag = false;
			if (search.getFrom().equals("")) {
				sql += " (SELECT created_date FROM contribution ORDER BY created_date LIMIT 1)";
			} else {
				sql += " ?";
				count += 1;
				fromFlag = true;
			}
			sql += " AND";
			if (search.getTo().equals("")) {
				sql += " (SELECT created_date FROM contribution ORDER BY created_date DESC LIMIT 1)";
			} else {
				sql += " ?";
				count += 1;
			}
			sql += " )";
			sql += " AND";
			sql += "  contribution.category LIKE ?";
			sql += " ORDER BY contribution.created_date DESC";
			ps = connection.prepareStatement(sql);
			if (count == 2) {
				if (fromFlag) ps.setString(1, search.getFrom());
				if (!fromFlag) ps.setString(1, search.getTo() + " 23:59:59");
			}
			if (count == 3) {
				ps.setString(1, search.getFrom());
				ps.setString(2, search.getTo() + " 23:59:59");
			}
			ps.setString(count, "%" + search.getCategory() + "%");
			ResultSet rs = ps.executeQuery();
			List<UserContribution> userContributionList = toUserContributionList(rs);
			if (userContributionList.isEmpty()) {
				return null;
			} else {
				return userContributionList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

    public void delete(Connection connection, int id) {
		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM contribution WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}