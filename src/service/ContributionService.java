package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Contribution;
import beans.Search;
import beans.UserContribution;
import dao.ContributionDao;
import dao.UserContributionDao;

public class ContributionService {

    public void register(Contribution contribution) {

        Connection connection = null;
        try {
            connection = getConnection();

            ContributionDao contibutionDao = new ContributionDao();
            contibutionDao.insert(connection, contribution);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<Contribution> findAll() {
		Connection connection = null;
		try {
			connection = getConnection();
			ContributionDao contributionDao = new ContributionDao();
			List<Contribution> contributionList = contributionDao.findAll(connection);
			commit(connection);
			return contributionList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

    private static final int LIMIT_NUM = 1000;

    public List<UserContribution> getContribution() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserContributionDao contibutionDao = new UserContributionDao();
    		List<UserContribution> ret = contibutionDao.getUserContribution(connection, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    public List<UserContribution> search(Search search) {
		Connection connection = null;
		try {
			connection = getConnection();
			ContributionDao contributionDao = new ContributionDao();
			List<UserContribution> UserContributionList = contributionDao.search(connection, search);
			commit(connection);
			return UserContributionList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

    public void delete(int id) {
		Connection connection = null;
		try {
			connection = getConnection();
			ContributionDao contributionDao = new ContributionDao();
			contributionDao.delete(connection, id);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

    public List<UserContribution> userContributionAll() {
		Connection connection = null;
		try {
			connection = getConnection();
			ContributionDao contributionDao = new ContributionDao();
			List<UserContribution> customPostList = contributionDao.userContributionAll(connection);
			commit(connection);
			return customPostList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}