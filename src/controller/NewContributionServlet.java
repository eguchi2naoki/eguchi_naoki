package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Contribution;
import beans.User;
import service.ContributionService;

@WebServlet(urlPatterns = { "/newContribution" })
public class NewContributionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");

		if(user != null) {
			request.getRequestDispatcher("contribution.jsp").forward(request, response);
		}else {
			List<String> messages = new ArrayList<String>();
			messages.add("ログインしてください!");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("login");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Contribution contribution = new Contribution();
			contribution.setTitle(request.getParameter("title"));
			contribution.setCategory(request.getParameter("category"));
			contribution.setText(request.getParameter("text"));
			contribution.setUserId(user.getId());

			new ContributionService().register(contribution);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("newContribution");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String text = request.getParameter("text");

		if (StringUtils.isEmpty(title) == true) {
			messages.add("件名を入力してください!");
		}
		if (30 < title.length()) {
			messages.add("件名は30文字以下で入力してください!");
		}
		if (StringUtils.isEmpty(category) == true) {
			messages.add("カテゴリーを入力してください!");
		}
		if (10 < category.length()) {
			messages.add("カテゴリーは10文字以下で入力してください!");
		}
		if (StringUtils.isEmpty(text) == true) {
			messages.add("本文を入力してください!");
		}
		if (1000 < text.length()) {
			messages.add("本文は1000文字以下で入力してください!");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}