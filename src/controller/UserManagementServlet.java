
package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ComplementUser;
import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/userManagement" })
public class UserManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;



	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");

		if(user == null) {
			List<String> messages = new ArrayList<String>();
			messages.add("ログインしてください!");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("login");
		}else if(user.getDepartmentId() != 1) {
			List<String> messages = new ArrayList<String>();
			messages.add("権限がないためアクセスできません!");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}else {
			//一覧表示
			List<ComplementUser> userList = new UserService().getAllComplementUser();
			request.setAttribute("userList", userList);
			request.getRequestDispatcher("usermanagement.jsp").forward(request, response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
	}
}