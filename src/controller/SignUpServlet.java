package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");

		if(user == null) {
			List<String> messages = new ArrayList<String>();
			messages.add("ログインしてください!");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("login");
		}else if(user.getDepartmentId() != 1)  {
			List<String> messages = new ArrayList<String>();
			messages.add("権限がないためアクセスできません!");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}else {
			Map<String, String> branchMap = new BranchService().findAll();
			Map<String, String> departmentMap = new DepartmentService().findAll();
			session.setAttribute("branch", branchMap);
			session.setAttribute("department", departmentMap);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		User user = new User();
		if (isValid(request, messages) == true) {

			user.setLoginId(request.getParameter("login_id"));
			user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
			user.setDepartmentId(Integer.parseInt(request.getParameter("department_id")));
			user.setName(request.getParameter("name"));
			user.setPassword(request.getParameter("password"));

			new UserService().register(user);
			session.removeAttribute("branch");
			session.removeAttribute("department");
			response.sendRedirect("userManagement");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", user);
			response.sendRedirect("signup");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("login_id");
		String name =request.getParameter("name");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("password_confirm");
		boolean passwordFlag = false;
		boolean passwordConfirmFlag = false;

		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("IDを入力してください!");
		}else if (!loginId.matches("[a-zA-Z0-9]{6,20}")) {
			messages.add("ログインIDは半角英数字6文字以上20文字以下で入力してください!");
		}
		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください!");
		}else if (name.length() > 10) {
			messages.add("名前は10文字以下で入力してください!");
		}
		if (StringUtils.isEmpty(password) == true) {
			messages.add("passを入力してください!");
		}else if (!password.matches("[ -~]{6,255}")) {
			messages.add("パスワードは半角文字6文字以上255文字以下で入力してください!");
		} else {
			passwordFlag = true;
		}
		if (StringUtils.isEmpty(passwordConfirm)) {
			messages.add("パスワード(確認用)を入力してください!");
		} else if (!passwordConfirm.matches("[ -~]{6,255}")) {
			messages.add("パスワード(確認用)は半角文字6文字以上255文字以下で入力してください!");
		} else {
			passwordConfirmFlag = true;
		}
		if (passwordFlag && passwordConfirmFlag) {
			if (!password.equals(passwordConfirm)) {
				messages.add("パスワードとパスワード(確認用)の値が不一致です!");
			}
		}

		if( messages.size() == 0) {
			return true;
		}else {
			return false;
		}

	}

}