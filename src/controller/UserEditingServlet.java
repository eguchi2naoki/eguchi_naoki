package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/userediting" })
public class UserEditingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		String id = request.getParameter("id");
		User user = (User) session.getAttribute("loginUser");

		if(user == null) {
			List<String> messages = new ArrayList<String>();
			messages.add("ログインしてください!");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("login");
		}else if(user.getDepartmentId() != 1) {
			List<String> messages = new ArrayList<String>();
			messages.add("権限がないためアクセスできません!");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}else {
			Map<String, String> branchMap = new BranchService().findAll();
			Map<String, String> departmentMap = new DepartmentService().findAll();
			session.setAttribute("branch", branchMap);
			session.setAttribute("department", departmentMap);

			int userId = 0;
			userId = Integer.parseInt(id);
			User users = new UserService().getUser(userId);
			session.setAttribute("userId", id);
			request.setAttribute("editUser", users);
			request.getRequestDispatcher("userediting.jsp").forward(request, response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		User user = setInputUser(request);
		UserService userService = new UserService();

		if (isValid(request, messages)) {


			userService.update(user);
			session.removeAttribute("branch");
			session.removeAttribute("department");
			session.removeAttribute("userId");
			response.sendRedirect("userManagement");
		} else {
			request.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", user);
			request.getRequestDispatcher("userediting.jsp").forward(request, response);
		}
	}

	private User setInputUser(HttpServletRequest request) {

		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setLoginId(request.getParameter("login_id"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("department_id")));

		return user;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("password_confirm");
		String name = request.getParameter("name");
		boolean passwordFlag = false;
		boolean passwordConfirmFlag = false;

		if (StringUtils.isEmpty(loginId)) {
			messages.add("ログインIDを入力してください!");
		} else if (!loginId.matches("[a-zA-Z0-9]{6,20}")) {
			messages.add("ログインIDは半角英数字6文字以上20文字以下で入力してください!");
		}
		if (!StringUtils.isEmpty(password)) {
			if (!password.matches("[ -~]{6,255}")) {
				messages.add("パスワードは半角文字6文字以上255文字以下で入力してください!");
			} else {
				passwordFlag = true;
			}
		}
		if (passwordFlag) {
			if (StringUtils.isEmpty(passwordConfirm)) {
				messages.add("パスワード(確認用)を入力してください!");
			} else if (!passwordConfirm.matches("[ -~]{6,255}")) {
				messages.add("パスワード(確認用)は半角文字6文字以上255文字以下で入力してください!");
			} else {
				passwordConfirmFlag = true;
			}
		} else {
			if (!StringUtils.isEmpty(passwordConfirm)) {
				messages.add("パスワードを入力してください!");
				if (!passwordConfirm.matches("[ -~]{6,255}")) {
					messages.add("パスワード(確認用)は半角文字6文字以上255文字以下で入力してください!");
				}
			}
		}
		if (passwordFlag && passwordConfirmFlag) {
			if (!password.equals(passwordConfirm)) {
				messages.add("パスワードとパスワード(確認用)の値が不一致です!");
			}
		}
		if (StringUtils.isEmpty(name)) {
			messages.add("名前を入力してください!");
		} else if (name.length() > 10) {
			messages.add("名前は10文字以下で入力してください!");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
