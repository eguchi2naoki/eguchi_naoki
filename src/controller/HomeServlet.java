package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Search;
import beans.User;
import beans.UserComment;
import beans.UserContribution;
import service.CommentService;
import service.ContributionService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

		//ログインユーザー情報取得
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		request.setAttribute("loginUser", user);

		//検索してない場合の表示
		List<UserContribution> contributions = new ContributionService().getContribution();
		request.setAttribute("contribution", contributions);
		List<UserComment> comments = new CommentService().getComment();
		request.setAttribute("comment", comments);

		request.getRequestDispatcher("home.jsp").forward(request, response);
	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.setCharacterEncoding("UTF-8");

		boolean from = true;
		boolean to = true;
		Search search = setInputSearch(request);

		//検索欄に入力がない場合
		if (search.getCategory() == null && search.getFrom() == null && search.getTo() == null) {
			List<UserContribution> contributions = new ContributionService().getContribution();
			request.setAttribute("contribution", contributions);
			List<UserComment> comments = new CommentService().getComment();
			request.setAttribute("comment", comments);

			request.getRequestDispatcher("home.jsp").forward(request, response);
		} else {
			if (!StringUtils.isEmpty(search.getCategory())) {
				request.setAttribute("editCategory", search.getCategory());
			}
			if (!StringUtils.isEmpty(search.getFrom())) {
				request.setAttribute("editFrom", search.getFrom());
			}
			if (!StringUtils.isEmpty(search.getTo())) {
				request.setAttribute("editTo", search.getTo());
			}
			//該当の投稿をセット
			List<UserContribution> contributions = new ContributionService().search(search);
			List<UserComment> comments = new CommentService().getComment();
			request.setAttribute("contribution", contributions);
			request.setAttribute("comment", comments);

			//検索結果がない場合
			if (contributions == null) {
				List<String> messages = new ArrayList<String>();
				messages.add("検索結果はありません");
				request.setAttribute("errorMessages", messages);
			}
			request.getRequestDispatcher("home.jsp").forward(request, response);
		}
	}


	private Search setInputSearch(HttpServletRequest request) {
		Search search = new Search();
		search.setCategory(request.getParameter("category"));
		search.setFrom(request.getParameter("from"));
		search.setTo(request.getParameter("to"));
		return search;
	}
}

