package beans;

import java.io.Serializable;
import java.util.Date;

public class UserComment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String name;
    private int user_id;
    private int contribution_id;
    private String text;
    private Date created_date;

    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUserId() {
		return user_id;
	}
	public void setUserId(int user_id) {
		this.user_id = user_id;
	}
	  public int getContributionId() {
			return contribution_id;
		}
	public void setContributionId(int contribution_id) {
		this.contribution_id = contribution_id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

}