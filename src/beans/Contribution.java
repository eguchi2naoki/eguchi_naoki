package beans;

import java.io.Serializable;
import java.util.Date;

public class Contribution implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int user_id;
    private String title;
    private String category;
    private String text;
    private Date created_date;
    private Date updated_date;

	public int getId() {
		return id;
	}
    public void setId(int id) {
    	this.id = id;
    }
    public int getUserId() {
		return user_id;
	}
	public void setUserId(int user_id) {
		this.user_id = user_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreatedDate() {
		return created_date;
	}
	public void setCreatedDate(Date created_date) {
		this.created_date = created_date;
	}
	public Date getUpdatedDate() {
		return updated_date;
	}
	public void setUpdatedDate(Date updated_date) {
		this.updated_date = updated_date;
	}

}