package beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int user_id;
    private int contribution_id;
    private String text;
    private Date created_date;
    private Date updated_date;

	public int getId() {
		return id;
	}
    public void setId(int id) {
    	this.id = id;
    }
    public int getUserId() {
		return user_id;
	}
		public void setUserId(int user_id) {
			this.user_id = user_id;
		}
		 public int getContributionId() {
				return contribution_id;
			}
		public void setContributionId(int contribution_id) {
			this.contribution_id = contribution_id;
		}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreatedDate() {
		return created_date;
	}
	public void setCreatedDate(Date created_date) {
		this.created_date = created_date;
	}
	public Date getUpdatedDate() {
		return updated_date;
	}
	public void setUpdatedDate(Date updated_date) {
		this.updated_date = updated_date;
	}

}