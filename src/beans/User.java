package beans;

import java.io.Serializable;


public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String  login_id;
	private int branch_id;
	private int department_id;
	private boolean isStopped;
	private String password;
	private String name;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id =id;
	}
	public String getLoginId() {
		return login_id;
	}
	public void setLoginId(String login_id) {
		this.login_id =login_id;
	}
	public int getBranchId() {
		return branch_id;
	}
	public void setBranchId(int branch_id) {
		this.branch_id = branch_id;
	}
	public int getDepartmentId() {
		return department_id;
	}
	public void setDepartmentId(int department_id) {
		this.department_id = department_id;
	}
	public String  getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isStopped() {
		return isStopped;
	}
	public void setStopped(boolean isStopped) {
		this.isStopped = isStopped;
	}
}